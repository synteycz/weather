object BuildPlugins {
    private object Versions {
        const val gradleVersion = "4.1.1"
        const val kotlinVersion = "1.4.20"
    }

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.gradleVersion}"
    const val kotlinGradlePlugin =
        "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
    const val kotlinStdLib =
        "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinVersion}"
    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinKapt = "kotlin-kapt"
}

object AndroidSdk {
    const val min = 21
    const val compile = 30
    const val buildToolsVersion = "29.0.3"
    const val target = compile
}

object AndroidLibraries {
    private object Versions {
        const val appCompat = "1.3.0-alpha02"
        const val support = "1.0.0"
        const val material = "1.2.1"
        const val cardView = "1.0.0"
        const val recyclerView = "1.2.0-alpha06"
        const val core = "1.5.0-alpha04"
        const val lifecycle = "2.3.0-beta01"
        const val constraintLayout = "2.0.2"
        const val multiDex = "2.0.1"
        const val playServices = "17.1.0"
        const val preference = "1.1.0"
        const val savedState = "1.0.0-rc03"
        const val fragment = "1.3.0-beta01"
        const val activity = "1.2.0-beta01"
        const val swipeRefreshLayout = "1.1.0"
    }

    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val support = "androidx.legacy:legacy-support-v4:${Versions.support}"
    const val design = "com.google.android.material:material:${Versions.material}"
    const val cardView = "androidx.cardview:cardview:${Versions.cardView}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
    const val core = "androidx.core:core-ktx:${Versions.core}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val liveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
    const val reactiveStreams =
        "androidx.lifecycle:lifecycle-reactivestreams-ktx:${Versions.lifecycle}"
    const val multiDex = "androidx.multidex:multidex:${Versions.multiDex}"
    const val locationServices =
        "com.google.android.gms:play-services-location:${Versions.playServices}"
    const val preference = "androidx.preference:preference-ktx:${Versions.preference}"
    const val savedState =
        "androidx.lifecycle:lifecycle-viewmodel-savedstate:${Versions.savedState}"
    const val fragment = "androidx.fragment:fragment-ktx:${Versions.fragment}"
    const val activity = "androidx.activity:activity-ktx:${Versions.activity}"
    const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipeRefreshLayout}"
}

object NetworkLibraries {
    private object Versions {
        const val retrofit = "2.9.0"
        const val retrofitRxJava = "1.0.0"
        const val moshi = "1.9.2"
        const val logging = "4.9.0"
    }

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    const val retrofitRxJava =
        "com.jakewharton.retrofit:retrofit2-rxjava2-adapter:${Versions.retrofitRxJava}"
    const val moshi = "com.squareup.moshi:moshi-kotlin:${Versions.moshi}"
    const val moshiProcessor = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"

    const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.logging}"
}

object RxJava {
    private object Versions {
        const val rxJava = "2.2.20"
        const val rxAndroid = "2.1.1"
    }

    const val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
}

object Coroutines {
    private object Versions {
        const val coroutines = "1.3.9"
    }

    const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
    const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines}"
}

object Room {
    private object Versions {
        const val roomVersion = "2.2.5"
    }

    const val roomRuntime = "androidx.room:room-runtime:${Versions.roomVersion}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.roomVersion}"
    const val roomCoroutines = "androidx.room:room-ktx:${Versions.roomVersion}"
    const val roomRxjava = "androidx.room:room-rxjava2:${Versions.roomVersion}"
    const val roomTest = "androidx.room:room-testing:${Versions.roomVersion}"
}

object Dagger {
    private object Versions {
        const val dagger = "2.28.1"
        const val assistedInject = "0.5.0"
    }

    const val dagger = "com.google.dagger:dagger-android:${Versions.dagger}"
    const val daggerSupport = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    const val daggerAndroidProcessor =
        "com.google.dagger:dagger-android-processor:${Versions.dagger}"
    const val assistedInjectAnnotation =
        "com.squareup.inject:assisted-inject-annotations-dagger2:${Versions.assistedInject}"
    const val assistedInjectProcessor =
        "com.squareup.inject:assisted-inject-processor-dagger2:${Versions.assistedInject}"
    const val javaXannotation = "javax.annotation:jsr250-api:1.0"
    const val inject = "javax.inject:javax.inject:1"
}

object TestLibraries {
    private object Versions {
        const val junit4 = "4.13.1"
        const val testRunner = "1.2.0"
        const val espresso = "3.2.0"
        const val mockk = "1.10.2"
        const val truth = "1.1"
        const val arch = "2.1.0"
        const val core = "1.3.0"
    }

    const val junit4 = "junit:junit:${Versions.junit4}"
    const val core = "androidx.test:core:${Versions.core}"
    const val testRunner = "androidx.test:runner:${Versions.testRunner}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val mockk = "io.mockk:mockk:${Versions.mockk}"
    const val truth = "com.google.truth:truth:${Versions.truth}"
    const val arch = "androidx.arch.core:core-testing:${Versions.arch}"
}

object Plugins {
    object Versions {
        const val detekt = "1.14.2"
        const val ktlint = "9.2.1"
        const val versions = "0.33.0"
    }

    const val ktlint = "org.jlleitschuh.gradle.ktlint"
    const val detektGradlePlugin = "io.gitlab.arturbosch.detekt:detekt-gradle-plugin:${Versions.detekt}"
    const val detekt = "io.gitlab.arturbosch.detekt"
    const val versions = "com.github.ben-manes.versions"
}

object OtherLibraries {
    const val coil = "io.coil-kt:coil:1.0.0"
    const val timber = "com.jakewharton.timber:timber:4.7.1"
    const val threeTenAbp = "com.jakewharton.threetenabp:threetenabp:1.2.4"
}

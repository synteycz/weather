import org.jlleitschuh.gradle.ktlint.reporter.ReporterType

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath(BuildPlugins.androidGradlePlugin)
        classpath(BuildPlugins.kotlinGradlePlugin)
        classpath(Plugins.detektGradlePlugin)
    }
}

plugins {
    id(Plugins.detekt) version Plugins.Versions.detekt
    id(Plugins.ktlint) version Plugins.Versions.ktlint
    id(Plugins.versions) version Plugins.Versions.versions
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

detekt {
    version = Plugins.Versions.detekt
    input = files(rootDir, "buildSrc/")
    config = files("$rootDir/default-detekt-config.yml")
    failFast = false
}

subprojects {
    apply(plugin = Plugins.ktlint)

    ktlint {
        ignoreFailures.set(true)
        android.set(true)
        outputToConsole.set(true)
        reporters {
            reporter(ReporterType.PLAIN)
            reporter(ReporterType.CHECKSTYLE)
        }
        disabledRules.set(setOf("no-wildcard-imports"))
    }
}

ktlint {
    ignoreFailures.set(true)
    android.set(true)
    outputToConsole.set(true)
    reporters {
        reporter(ReporterType.PLAIN)
        reporter(ReporterType.CHECKSTYLE)
    }
    disabledRules.set(setOf("no-wildcard-imports"))
}

tasks.register("clean").configure {
    delete("build")
}

tasks.withType<com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask> {
    fun isNonStable(version: String): Boolean {
        val stableKeyword =
            listOf("RELEASE", "FINAL", "GA").any { version.toUpperCase().contains(it) }
        val regex = "^[0-9,.v-]+(-r)?$".toRegex()
        val isStable = stableKeyword || regex.matches(version)
        return isStable.not()
    }

    /*rejectVersionIf {
        isNonStable(candidate.version)
    }*/
}

package com.syntey.weather

import android.content.res.Resources
import android.location.Location
import android.text.format.DateFormat
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.syntey.weather.AppConstants.DEFAULT_LOCATION_LATITUDE
import com.syntey.weather.AppConstants.DEFAULT_LOCATION_LONGITUDE
import com.syntey.weather.domain.model.AppSettings
import com.syntey.weather.domain.model.Result
import com.syntey.weather.domain.model.Today
import com.syntey.weather.domain.repository.IAppStateRepository
import com.syntey.weather.domain.repository.ITextRepository
import com.syntey.weather.domain.usecase.ForecastWeatherUseCase
import com.syntey.weather.domain.usecase.TodayWeatherUseCase
import com.syntey.weather.ui.WeatherViewModel
import com.syntey.weather.ui.base.ViewState
import com.syntey.weather.utils.getValueForTest
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.reactivex.Completable
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class TodayUnitTest {
    private lateinit var viewModel: WeatherViewModel
    private val appStateRepository: IAppStateRepository = mockk()
    private val textRepository: ITextRepository = mockk()
    private val forecastWeatherUseCase: ForecastWeatherUseCase = mockk()
    private val todayWeatherUseCase: TodayWeatherUseCase = mockk()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        every { appStateRepository.populateIfEmpty(any()) } returns Completable.complete()
        viewModel = WeatherViewModel(
            todayWeatherUseCase,
            forecastWeatherUseCase,
            appStateRepository,
            textRepository
        )

        mockkStatic(DateFormat::class)
        every { DateFormat.format(any(), any<Long>()) } returns ""

        mockkStatic(Resources::class)
        every { Resources.getSystem().getString(any()) } returns "Text"
    }

    @Test
    fun `When repository returned loading should show loading in UI`() {
        every { todayWeatherUseCase.invoke(any()) } returns Flowable.just(
            Result.loading()
        )
        every { forecastWeatherUseCase.invoke(any()) } returns Flowable.just(
            Result.loading()
        )
        every { appStateRepository.getMetricSystem() } returns Flowable.just(
            AppSettings.MetricSystem.METRIC
        )

        val location: Location = mockk()
        every { location.latitude } returns DEFAULT_LOCATION_LATITUDE
        every { location.longitude } returns DEFAULT_LOCATION_LONGITUDE
        viewModel.updateLocation(location)

        assertThat(viewModel.todayViewState.getValueForTest() is ViewState.Loading).isTrue()
    }

    @Test
    fun `When repository returned data should show data in UI`() {
        every { todayWeatherUseCase.invoke(any()) } returns Flowable.just(
            Result.success(
                Today().apply {
                    city = "Brno"
                }
            )
        )
        every { forecastWeatherUseCase.invoke(any()) } returns Flowable.just(
            Result.loading()
        )
        every { appStateRepository.getMetricSystem() } returns Flowable.just(
            AppSettings.MetricSystem.METRIC
        )

        val location: Location = mockk()
        every { location.latitude } returns DEFAULT_LOCATION_LATITUDE
        every { location.longitude } returns DEFAULT_LOCATION_LONGITUDE
        viewModel.updateLocation(location)

        assertThat(viewModel.todayViewState.getValueForTest() is ViewState.Data).isTrue()
        assertThat((viewModel.todayViewState.getValueForTest() as ViewState.Data).data.city)
            .isEqualTo("Brno")
    }

    @Test
    fun `When repository returned error should show error in UI`() {
        every { todayWeatherUseCase.invoke(any()) } returns Flowable.just(
            Result.error(404, "")
        )
        every { forecastWeatherUseCase.invoke(any()) } returns Flowable.just(
            Result.loading()
        )
        every { appStateRepository.getMetricSystem() } returns Flowable.just(
            AppSettings.MetricSystem.METRIC
        )
        every { textRepository.getString(any()) } returns "Error"

        val location: Location = mockk()
        every { location.latitude } returns DEFAULT_LOCATION_LATITUDE
        every { location.longitude } returns DEFAULT_LOCATION_LONGITUDE
        viewModel.updateLocation(location)

        assertThat(viewModel.todayViewState.getValueForTest() is ViewState.Error).isTrue()
        assertThat((viewModel.todayViewState.getValueForTest() as ViewState.Error).error).isEqualTo(
            "Error"
        )
    }
}

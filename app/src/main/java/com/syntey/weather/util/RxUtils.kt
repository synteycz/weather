package com.syntey.weather.util

import android.annotation.SuppressLint
import io.reactivex.Completable
import io.reactivex.observers.DisposableCompletableObserver
import timber.log.Timber

@SuppressLint("CheckResult")
fun Completable.subscribeDisposable(onComplete: () -> Unit = {}) {
    this.subscribeWith(
        object : DisposableCompletableObserver() {
            override fun onComplete() {
                onComplete()
                this.dispose()
            }

            override fun onError(e: Throwable) {
                Timber.e(e)
                this.dispose()
            }
        }
    )
}

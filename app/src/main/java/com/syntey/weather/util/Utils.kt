package com.syntey.weather.util

import com.syntey.weather.R
import com.syntey.weather.domain.model.ErrorWrapper
import com.syntey.weather.domain.repository.ITextRepository

object Utils {
    fun parseError(error: ErrorWrapper, textRepository: ITextRepository): String {
        return when (error.code) {
            404 -> textRepository.getString(R.string.error_message_not_found)
            500 -> textRepository.getString(R.string.error_message_server_not_responding)
            else -> {
                if (error.message.contains("Unable to resolve host")) {
                    textRepository.getString(R.string.error_message_no_internet)
                } else {
                    String.format(
                        "%s %s",
                        textRepository.getString(R.string.error_message_unknown),
                        error.message
                    )
                }
            }
        }
    }

    fun getWindDegreeSymbol(deg: Int): String {
        return when (deg) {
            in 0..30 -> {
                "N"
            }
            in 30..75 -> {
                "NE"
            }
            in 75..120 -> {
                "E"
            }
            in 120..165 -> {
                "SE"
            }
            in 165..210 -> {
                "S"
            }
            in 210..255 -> {
                "SW"
            }
            in 255..300 -> {
                "W"
            }
            in 300..345 -> {
                "NW"
            }
            in 345..360 -> {
                "N"
            }
            else -> ""
        }
    }
}

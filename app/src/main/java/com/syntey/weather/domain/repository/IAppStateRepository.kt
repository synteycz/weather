package com.syntey.weather.domain.repository

import com.syntey.weather.domain.model.AppSettings
import io.reactivex.Completable
import io.reactivex.Flowable

interface IAppStateRepository {
    fun getMetricSystem(): Flowable<AppSettings.MetricSystem>
    fun populateIfEmpty(appSettings: AppSettings): Completable
    fun updateMetricSystem(metricSystem: AppSettings.MetricSystem): Completable
}

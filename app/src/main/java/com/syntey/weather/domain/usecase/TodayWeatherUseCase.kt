package com.syntey.weather.domain.usecase

import com.syntey.weather.domain.model.Result
import com.syntey.weather.domain.model.Today
import com.syntey.weather.domain.repository.IAppStateRepository
import com.syntey.weather.domain.repository.IWeatherRepository
import io.reactivex.Flowable
import javax.inject.Inject

data class TodayWeatherUseCaseParams(
    val latitude: Double,
    val longitude: Double
)

class TodayWeatherUseCase @Inject constructor(
    private val appStateRepository: IAppStateRepository,
    private val weatherRepository: IWeatherRepository
) : FlowableUseCase<TodayWeatherUseCaseParams, Today> {

    override fun execute(params: TodayWeatherUseCaseParams): Flowable<Result<Today>> {
        return appStateRepository.getMetricSystem().switchMap { metric ->
            weatherRepository.getTodayWeatherByLocation(
                params.latitude,
                params.longitude,
                metric.value
            ).toFlowable()
        }
    }
}

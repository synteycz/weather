package com.syntey.weather.domain.model

class Forecast {
    var weather: String = ""
    var time: Long = 0
    var temperature: Double = 0.0
    var weatherIcon: String = ""
}

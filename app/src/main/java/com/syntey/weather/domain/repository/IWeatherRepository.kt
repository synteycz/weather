package com.syntey.weather.domain.repository

import com.syntey.weather.domain.model.Forecast
import com.syntey.weather.domain.model.Result
import com.syntey.weather.domain.model.Today
import io.reactivex.Single

interface IWeatherRepository {
    fun getTodayWeatherByLocation(
        latitude: Double,
        longitude: Double,
        metric: String
    ): Single<Result<Today>>

    fun getForecastWeatherByLocation(
        latitude: Double,
        longitude: Double,
        metric: String
    ): Single<Result<List<Forecast>>>
}

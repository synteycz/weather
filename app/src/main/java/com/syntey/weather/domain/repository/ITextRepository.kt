package com.syntey.weather.domain.repository

import androidx.annotation.StringRes

interface ITextRepository {
    fun getString(@StringRes id: Int): String
}

package com.syntey.weather.domain.usecase

import com.syntey.weather.domain.model.Forecast
import com.syntey.weather.domain.model.Result
import com.syntey.weather.domain.repository.IAppStateRepository
import com.syntey.weather.domain.repository.IWeatherRepository
import io.reactivex.Flowable
import javax.inject.Inject

data class ForecastWeatherUseCaseParams(
    val latitude: Double,
    val longitude: Double
)

class ForecastWeatherUseCase @Inject constructor(
    private val appStateRepository: IAppStateRepository,
    private val weatherRepository: IWeatherRepository
) : FlowableUseCase<ForecastWeatherUseCaseParams, List<Forecast>> {
    override fun execute(params: ForecastWeatherUseCaseParams): Flowable<Result<List<Forecast>>> {
        return appStateRepository.getMetricSystem().switchMap { metric ->
            weatherRepository.getForecastWeatherByLocation(
                params.latitude,
                params.longitude,
                metric.value
            ).toFlowable()
        }
    }
}

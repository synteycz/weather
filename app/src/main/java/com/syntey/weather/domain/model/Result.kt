package com.syntey.weather.domain.model

sealed class Result<out T> {
    data class Success<T>(val data: T) : Result<T>()
    data class Error<T>(val error: ErrorWrapper) : Result<T>()

    class Loading<T> : Result<T>()

    companion object {
        fun <T> success(data: T): Result<T> {
            return Success(data)
        }

        fun <T> error(code: Int, msg: String): Result<T> {
            return Error(
                ErrorWrapper(code, msg)
            )
        }

        fun <T> loading(): Result<T> {
            return Loading()
        }
    }
}

data class ErrorWrapper(val code: Int, val message: String)

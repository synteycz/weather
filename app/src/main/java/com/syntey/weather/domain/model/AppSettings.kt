package com.syntey.weather.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "app_settings")
class AppSettings {
    @PrimaryKey
    var id: Int = 1
    @ColumnInfo(name = "metric_system")
    var metricSystem: MetricSystem = MetricSystem.METRIC

    enum class MetricSystem(val value: String) {
        METRIC("Metric"), IMPERIAL("Imperial")
    }
}

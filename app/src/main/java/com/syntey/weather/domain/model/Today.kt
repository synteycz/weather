package com.syntey.weather.domain.model

class Today {
    var weatherMain: String = ""
    var weatherDescription: String = ""
    var weatherIcon: String = ""
    var temperature: Double = 0.0
    var pressure: Int = 0
    var humidity: Int = 0
    var windSpeed: Double = 0.0
    var windDeg: String = ""
    var clouds: Int = 0
    var rain: Double = 0.0
    var sunrise: Long = 0
    var sunset: Long = 0
    var city: String = ""
}

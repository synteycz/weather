package com.syntey.weather.domain.usecase

import com.syntey.weather.AppConstants
import com.syntey.weather.domain.model.Result
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers

interface FlowableUseCase<in P, R> {
    operator fun invoke(params: P): Flowable<Result<R>> = execute(params)
        .onErrorReturn { t: Throwable ->
            Result.error(
                AppConstants.UNKNOWN_ERROR,
                t.localizedMessage ?: ""
            )
        }
        .startWith(Result.loading())
        .subscribeOn(Schedulers.io())

    fun execute(params: P): Flowable<Result<R>>
}

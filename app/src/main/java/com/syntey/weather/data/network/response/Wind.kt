package com.syntey.weather.data.network.response

import com.squareup.moshi.Json

data class Wind(
    val speed: Double,
    @field:Json(name = "deg")
    val degree: Int
)

package com.syntey.weather.data.repository

import com.syntey.weather.AppConstants
import com.syntey.weather.data.mappers.ForecastWeatherMapper
import com.syntey.weather.data.network.ApiService
import com.syntey.weather.domain.model.Forecast
import com.syntey.weather.domain.model.Result
import io.reactivex.Single

class ForecastWeatherNetworkDataSource(
    private val apiService: ApiService,
    private val forecastWeatherMapper: ForecastWeatherMapper
) {

    fun fetch(
        latitude: Double,
        longitude: Double,
        metric: String
    ): Single<Result<List<Forecast>>> {
        return apiService.getForecastWeatherByLocation(latitude, longitude, metric)
            .map { response ->
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        Result.success(forecastWeatherMapper.map(response.body()!!))
                    } else {
                        Result.error(
                            AppConstants.UNKNOWN_ERROR,
                            "No valid response from server"
                        )
                    }
                } else {
                    Result.error(response.code(), response.message())
                }
            }
    }
}

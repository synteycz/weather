package com.syntey.weather.data.network.response

data class Weather(
    val main: String,
    val description: String,
    val icon: String
)

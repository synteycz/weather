package com.syntey.weather.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.syntey.weather.domain.model.AppSettings

@Database(entities = [(AppSettings::class)], version = 1)
@TypeConverters(RoomConverters::class)
abstract class DB : RoomDatabase() {
    abstract fun appStateDao(): AppStateDao
}

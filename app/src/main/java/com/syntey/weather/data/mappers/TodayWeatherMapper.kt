package com.syntey.weather.data.mappers

import com.syntey.weather.data.network.response.CurrentWeather
import com.syntey.weather.domain.model.Today
import com.syntey.weather.util.Utils
import java.util.Locale

class TodayWeatherMapper {
    fun map(currentWeather: CurrentWeather): Today {
        return Today().apply {
            currentWeather.weather.firstOrNull()?.let {
                this.weatherMain = it.main

                val description = it.description
                if (description.isNotBlank())
                    this.weatherDescription =
                        description.substring(0, 1)
                        .toUpperCase(Locale.getDefault()) + description.substring(1)

                this.weatherIcon = it.icon
            }
            currentWeather.mainInfo.let {
                this.temperature = it.temp
                this.pressure = it.pressure
                this.humidity = it.humidity
            }

            currentWeather.wind.let {
                this.windSpeed = it.speed
                this.windDeg = Utils.getWindDegreeSymbol(it.degree)
            }

            this.clouds = currentWeather.clouds.cloudiness
            this.rain = currentWeather.rain?.last3Hours ?: 0.0

            currentWeather.dayTimes.let {
                this.sunrise = it.sunrise
                this.sunset = it.sunset
            }

            this.city = currentWeather.name
        }
    }
}

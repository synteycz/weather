package com.syntey.weather.data.network.response

import com.squareup.moshi.Json

data class Clouds(
    @field:Json(name = "all")
    val cloudiness: Int
)

package com.syntey.weather.data.network.response

import com.squareup.moshi.Json

data class CurrentWeather(
    val weather: List<Weather>,
    @field:Json(name = "main")
    val mainInfo: MainInfo,
    val wind: Wind,
    val rain: Rain?,
    val clouds: Clouds,
    @field:Json(name = "sys")
    val dayTimes: DayTimes,
    val name: String
)

package com.syntey.weather.data.repository

import com.syntey.weather.data.db.AppStateDao
import com.syntey.weather.data.db.DB
import com.syntey.weather.domain.model.AppSettings
import com.syntey.weather.domain.repository.IAppStateRepository
import io.reactivex.Completable
import io.reactivex.Flowable

class AppStateRepository(private val db: DB) :
    IAppStateRepository {
    private val appStateDao: AppStateDao = db.appStateDao()

    override fun populateIfEmpty(appSettings: AppSettings): Completable {
        return Completable.fromCallable {
            db.runInTransaction {
                if (appStateDao.getAppState().isEmpty()) {
                    appStateDao.insert(appSettings)
                }
            }
        }
    }

    override fun getMetricSystem(): Flowable<AppSettings.MetricSystem> {
        return appStateDao.getMetricSystem()
    }

    override fun updateMetricSystem(metricSystem: AppSettings.MetricSystem): Completable {
        return appStateDao.updateMetricType(metricSystem)
    }
}

package com.syntey.weather.data.network.response

import com.squareup.moshi.Json

data class MainInfo(
    val temp: Double,
    val pressure: Int,
    val humidity: Int,
    @field:Json(name = "temp_min")
    val tempMin: Double,
    @field:Json(name = "temp_max")
    val tempMax: Double
)

package com.syntey.weather.data.repository

import android.content.Context
import androidx.annotation.StringRes
import com.syntey.weather.domain.repository.ITextRepository

class TextRepository(private val context: Context) :
    ITextRepository {

    override fun getString(@StringRes id: Int): String {
        return context.getString(id)
    }
}

package com.syntey.weather.data.mappers

import com.syntey.weather.data.network.response.ForecastCollection
import com.syntey.weather.domain.model.Forecast
import java.util.Locale

class ForecastWeatherMapper {
    fun map(forecastCollection: ForecastCollection): List<Forecast> {
        return forecastCollection.forecastCollection.map {
            Forecast().apply {
                val description = it.weather.firstOrNull()?.description
                if (!description.isNullOrBlank())
                    this.weather =
                        description.substring(0, 1)
                        .toUpperCase(Locale.getDefault()) + description.substring(1)
                this.weatherIcon = it.weather.firstOrNull()?.icon ?: ""
                this.temperature = it.mainInfo.temp
                this.time = it.timeOfData * 1000
            }
        }
    }
}

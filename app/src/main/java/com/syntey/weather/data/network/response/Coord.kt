package com.syntey.weather.data.network.response

data class Coord(
    val lon: Double,
    val lat: Double
)

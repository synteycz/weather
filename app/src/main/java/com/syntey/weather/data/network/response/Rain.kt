package com.syntey.weather.data.network.response

import com.squareup.moshi.Json

data class Rain(
    @field:Json(name = "1h")
    val lastHour: Double,
    @field:Json(name = "3h")
    val last3Hours: Double
)

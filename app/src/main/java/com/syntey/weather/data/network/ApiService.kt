package com.syntey.weather.data.network

import com.syntey.weather.data.network.response.CurrentWeather
import com.syntey.weather.data.network.response.ForecastCollection
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("weather")
    fun getTodayWeatherByLocation(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("units") units: String
    ): Single<Response<CurrentWeather>>

    @GET("forecast")
    fun getForecastWeatherByLocation(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("units") units: String
    ): Single<Response<ForecastCollection>>
}

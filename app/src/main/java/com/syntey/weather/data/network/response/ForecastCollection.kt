package com.syntey.weather.data.network.response

import com.squareup.moshi.Json

data class ForecastCollection(
    @field:Json(name = "list")
    val forecastCollection: List<Forecast>
)

package com.syntey.weather.data.db

import androidx.room.TypeConverter
import com.syntey.weather.domain.model.AppSettings

object RoomConverters {
    @TypeConverter
    @JvmStatic
    fun stringToMetricSystem(metricValue: String): AppSettings.MetricSystem {
        return AppSettings.MetricSystem.valueOf(metricValue)
    }

    @TypeConverter
    @JvmStatic
    fun metricSystemToString(metricSystem: AppSettings.MetricSystem): String {
        return metricSystem.name
    }
}

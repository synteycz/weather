package com.syntey.weather.data.repository

import com.syntey.weather.domain.model.Forecast
import com.syntey.weather.domain.model.Result
import com.syntey.weather.domain.model.Today
import com.syntey.weather.domain.repository.IWeatherRepository
import io.reactivex.Single

class WeatherRepository(
    private val todayWeatherNetworkDataSource: TodayWeatherNetworkDataSource,
    private val forecastWeatherNetworkDataSource: ForecastWeatherNetworkDataSource
) : IWeatherRepository {

    override fun getTodayWeatherByLocation(
        latitude: Double,
        longitude: Double,
        metric: String
    ): Single<Result<Today>> {
        return todayWeatherNetworkDataSource.fetch(
            latitude,
            longitude,
            metric
        )
    }

    override fun getForecastWeatherByLocation(
        latitude: Double,
        longitude: Double,
        metric: String
    ): Single<Result<List<Forecast>>> {
        return forecastWeatherNetworkDataSource.fetch(
            latitude,
            longitude,
            metric
        )
    }
}

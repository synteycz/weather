package com.syntey.weather.data.network.response

import com.squareup.moshi.Json

data class Forecast(
    @field:Json(name = "dt")
    val timeOfData: Long,

    @field:Json(name = "main")
    val mainInfo: MainInfo,

    @field:Json(name = "weather")
    val weather: List<Weather>
)

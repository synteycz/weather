package com.syntey.weather.data.repository

import com.syntey.weather.AppConstants
import com.syntey.weather.data.mappers.TodayWeatherMapper
import com.syntey.weather.data.network.ApiService
import com.syntey.weather.domain.model.Result
import com.syntey.weather.domain.model.Today
import io.reactivex.Single

class TodayWeatherNetworkDataSource(
    private val apiService: ApiService,
    private val todayWeatherMapper: TodayWeatherMapper
) {

    fun fetch(
        latitude: Double,
        longitude: Double,
        metric: String
    ): Single<Result<Today>> {
        return apiService.getTodayWeatherByLocation(latitude, longitude, metric)
            .map { response ->
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        Result.success(todayWeatherMapper.map(response.body()!!))
                    } else {
                        Result.error(
                            AppConstants.UNKNOWN_ERROR,
                            "No valid response from server"
                        )
                    }
                } else {
                    Result.error(response.code(), response.message())
                }
            }
    }
}

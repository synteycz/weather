package com.syntey.weather.data.network.response

data class DayTimes(
    val sunrise: Long,
    val sunset: Long
)

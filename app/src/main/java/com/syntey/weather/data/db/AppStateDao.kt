package com.syntey.weather.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.syntey.weather.domain.model.AppSettings
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface AppStateDao {

    @Query("SELECT metric_system FROM app_settings")
    fun getMetricSystem(): Flowable<AppSettings.MetricSystem>

    @Query("SELECT * FROM app_settings")
    fun getAppState(): List<AppSettings>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(appSettings: AppSettings)

    @Update
    fun update(appSettings: AppSettings): Completable

    @Query("UPDATE app_settings SET metric_system = :metricSystem")
    fun updateMetricType(metricSystem: AppSettings.MetricSystem): Completable
}

package com.syntey.weather

object AppConstants {
    const val IMAGE_URL = "http://openweathermap.org/img/wn/"
    const val IMAGE_FILE_TYPE = ".png"
    const val PERMISSIONS_REQUEST_LOCATION = 44
    const val DEFAULT_LOCATION_LATITUDE = 49.19
    const val DEFAULT_LOCATION_LONGITUDE = 16.61

    const val UNKNOWN_ERROR = 5000
}

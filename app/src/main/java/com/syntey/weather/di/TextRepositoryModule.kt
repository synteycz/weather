package com.syntey.weather.di

import android.content.Context
import com.syntey.weather.data.repository.TextRepository
import com.syntey.weather.domain.repository.ITextRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object TextRepositoryModule {

    @Provides
    @Singleton
    fun provideWeatherRepository(
        context: Context
    ): ITextRepository = TextRepository(context)
}

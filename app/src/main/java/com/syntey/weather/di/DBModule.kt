package com.syntey.weather.di

import android.app.Application
import androidx.room.Room
import com.syntey.weather.data.db.DB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DBModule {

    @Provides
    @Singleton
    fun provideDatabase(application: Application): DB {
        return Room.databaseBuilder(
            application,
            DB::class.java,
            "database.db"
        ).build()
    }
}

@Module
object MockDBModule {

    @Provides
    @Singleton
    fun provideDatabase(application: Application): DB =
        Room.inMemoryDatabaseBuilder(application, DB::class.java).build()
}

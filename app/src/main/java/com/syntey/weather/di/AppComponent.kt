package com.syntey.weather.di

import android.app.Application
import com.syntey.weather.WeatherApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        AppModule::class,
        NetworkModule::class,
        DBModule::class,
        ActivityModule::class,
        WeatherRepositoryModule::class,
        AppStateRepositoryModule::class,
        TextRepositoryModule::class
    ]
)
@Singleton
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): AppComponent
    }

    fun inject(application: WeatherApplication)
}

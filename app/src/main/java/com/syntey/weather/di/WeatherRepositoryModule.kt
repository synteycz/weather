package com.syntey.weather.di

import com.syntey.weather.data.mappers.ForecastWeatherMapper
import com.syntey.weather.data.mappers.TodayWeatherMapper
import com.syntey.weather.data.network.ApiService
import com.syntey.weather.data.repository.ForecastWeatherNetworkDataSource
import com.syntey.weather.data.repository.TodayWeatherNetworkDataSource
import com.syntey.weather.data.repository.WeatherRepository
import com.syntey.weather.domain.repository.IWeatherRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object WeatherRepositoryModule {

    @Provides
    @Singleton
    fun provideWeatherRepository(apiService: ApiService): IWeatherRepository =
        WeatherRepository(
            TodayWeatherNetworkDataSource(apiService, TodayWeatherMapper()),
            ForecastWeatherNetworkDataSource(apiService, ForecastWeatherMapper())
        )
}

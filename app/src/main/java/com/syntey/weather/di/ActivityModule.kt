package com.syntey.weather.di

import com.syntey.weather.ui.WeatherActivity
import com.syntey.weather.ui.forecast.ForecastFragment
import com.syntey.weather.ui.settings.ChooseMetricDialog
import com.syntey.weather.ui.settings.SettingsActivity
import com.syntey.weather.ui.today.TodayFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): WeatherActivity

    @ContributesAndroidInjector
    abstract fun contributesTodayFragment(): TodayFragment

    @ContributesAndroidInjector
    abstract fun contributesForecastFragment(): ForecastFragment

    @ContributesAndroidInjector
    abstract fun contributesSettingsActivity(): SettingsActivity

    @ContributesAndroidInjector
    abstract fun contributesChooseMetricDialog(): ChooseMetricDialog
}

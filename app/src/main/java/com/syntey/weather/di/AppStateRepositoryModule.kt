package com.syntey.weather.di

import com.syntey.weather.data.db.DB
import com.syntey.weather.data.repository.AppStateRepository
import com.syntey.weather.domain.repository.IAppStateRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object AppStateRepositoryModule {
    @Provides
    @Singleton
    fun provideAppStateRepository(db: DB): IAppStateRepository = AppStateRepository(db)
}

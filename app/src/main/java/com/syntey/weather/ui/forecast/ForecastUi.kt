package com.syntey.weather.ui.forecast

import androidx.recyclerview.widget.DiffUtil

data class ForecastUi(
    val weatherDescription: String,
    val temperature: String,
    val iconUrl: String
) : DiffUtil.ItemCallback<ForecastUi>() {
    override fun areItemsTheSame(oldItem: ForecastUi, newItem: ForecastUi): Boolean {
        // normally would be id, but that's not possible
        return oldItem.weatherDescription == newItem.weatherDescription
    }

    override fun areContentsTheSame(oldItem: ForecastUi, newItem: ForecastUi): Boolean {
        return oldItem.temperature == newItem.temperature && oldItem.iconUrl == newItem.iconUrl
    }
}

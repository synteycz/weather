package com.syntey.weather.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.tabs.TabLayoutMediator
import com.syntey.weather.R
import com.syntey.weather.databinding.WeatherActivityBinding
import com.syntey.weather.ui.base.BaseActivity
import com.syntey.weather.ui.settings.SettingsActivity

class WeatherActivity : BaseActivity<WeatherActivityBinding>() {

    private val mainViewModel by viewModels<WeatherViewModel>()

    private val requestPermissionLauncher = registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                mainViewModel.permissionsGranted()
            } else {
                mainViewModel.permissionsNotGranted()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViewPager()
        setSupportActionBar(binding.toolbar)
        initializeLocation()
    }

    private fun setupViewPager() {
        val sectionsPagerAdapter = PageAdapter(
            this, 2
        )

        binding.viewPager.adapter = sectionsPagerAdapter
        binding.viewPager.offscreenPageLimit = 2
        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            when (position) {
                0 -> {
                    tab.icon = ContextCompat.getDrawable(this, R.drawable.ic_wb_sunny_24px)
                    tab.text = getString(R.string.tab_text_1)
                }
                1 -> {
                    tab.icon = ContextCompat.getDrawable(this, R.drawable.ic_event_24px)
                    tab.text = getString(R.string.tab_text_2)
                }
            }
        }.attach()
    }

    private fun initializeLocation() {
        mainViewModel.initLocation(applicationContext)

        mainViewModel.showLocationNotGrantedDialog.observe(this, {
            showLocationNotGrantedDialog()
        })

        requestLocationPermission()
    }

    private fun requestLocationPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mainViewModel.permissionsGranted()
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            mainViewModel.locationFailed()
                } else {
                    requestPermissionLauncher.launch(
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                }
            } else {
                requestPermissionLauncher.launch(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mainViewModel.onResume()
    }

    override fun onPause() {
        super.onPause()
        mainViewModel.onPause()
    }

    private fun showLocationNotGrantedDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.location_disabled_dialog_title)
            .setMessage(getString(R.string.location_disabled_dialog_message))
            .setPositiveButton(
                getString(R.string.location_disabled_dialog_confirm)) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            R.id.action_about -> {
                showAboutDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showAboutDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.about)
            .setMessage(getString(R.string.about_dialog_message))
            .setPositiveButton(getString(R.string.dialog_confirm)) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    override fun getBindings(layoutInflater: LayoutInflater): WeatherActivityBinding {
        return WeatherActivityBinding.inflate(layoutInflater)
    }
}

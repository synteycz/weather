package com.syntey.weather.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import androidx.activity.viewModels
import com.syntey.weather.databinding.SettingsActivityBinding
import com.syntey.weather.ui.base.BaseActivity

class SettingsActivity : BaseActivity<SettingsActivityBinding>() {

    private val viewModel by viewModels<SettingsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.unitOfLengthLayout.setOnClickListener {
            viewModel.showDialog()
        }

        viewModel.metricSystem.observe(this, {
            binding.unitOfLengthValue.text = it.value
        })

        viewModel.showDialog.observe(this, { selectedMetricSystem ->
            val dialog = ChooseMetricDialog.newInstance(selectedMetricSystem)
            dialog.show(supportFragmentManager, "chooseMetric")
        })

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun getBindings(layoutInflater: LayoutInflater): SettingsActivityBinding {
        return SettingsActivityBinding.inflate(layoutInflater)
    }
}

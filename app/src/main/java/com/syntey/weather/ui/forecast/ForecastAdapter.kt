package com.syntey.weather.ui.forecast

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.syntey.weather.databinding.ForecastViewHolderBinding

class ForecastAdapter : ListAdapter<ForecastUi, ForecastAdapter.ForecastViewHolder>(
    object : DiffUtil.ItemCallback<ForecastUi>() {
        override fun areItemsTheSame(oldItem: ForecastUi, newItem: ForecastUi): Boolean {
            return oldItem.areItemsTheSame(oldItem, newItem)
        }

        override fun areContentsTheSame(oldItem: ForecastUi, newItem: ForecastUi): Boolean {
            return oldItem.areContentsTheSame(oldItem, newItem)
        }
    }
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        return ForecastViewHolder(
            ForecastViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        val item = getItem(position)

        holder.forecastDescription.text = item.weatherDescription
        holder.forecastIcon.load(item.iconUrl)
        holder.forecastTemperature.text = item.temperature
    }

    class ForecastViewHolder(view: ForecastViewHolderBinding) : RecyclerView.ViewHolder(view.root) {
        val forecastIcon: ImageView = view.forecastIcon
        val forecastDescription: TextView = view.forecastDescription
        val forecastTemperature: TextView = view.forecastTemperature
    }
}

package com.syntey.weather.ui.base

sealed class ViewState<T> {
    data class Data<T>(val data: T) : ViewState<T>()
    class Loading<T> : ViewState<T>()
    data class Error<T>(val error: String) : ViewState<T>()

    companion object {
        fun <T> data(data: T): ViewState<T> {
            return Data(data)
        }

        fun <T> error(error: String): ViewState<T> {
            return Error(error)
        }

        fun <T> loading(): ViewState<T> {
            return Loading()
        }
    }
}

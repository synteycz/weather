package com.syntey.weather.ui.settings

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.activityViewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.syntey.weather.R
import com.syntey.weather.domain.model.AppSettings
import dagger.android.support.DaggerDialogFragment

class ChooseMetricDialog : DaggerDialogFragment() {

    private val viewModel by activityViewModels<SettingsViewModel>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val selectedMetric = AppSettings.MetricSystem.valueOf(
            arguments?.getString(BUNDLE_KEY) ?: AppSettings.MetricSystem.METRIC.name
        )

        val builder = MaterialAlertDialogBuilder(requireContext())
        builder.setTitle(getString(R.string.choose_metric_system))

        builder.setSingleChoiceItems(
            R.array.lengthList,
            selectedMetric.ordinal,
            null
        )

        builder.setPositiveButton(R.string.dialog_confirm) { dialogInterface, _ ->
            val itemId = (dialogInterface as AlertDialog).listView.checkedItemPosition
            viewModel.dialogConfirmed(itemId)

            dialogInterface.dismiss()
        }

        return builder.setNegativeButton(R.string.cancel_dialog, null).create()
    }

    companion object {
        const val BUNDLE_KEY = "METRIC"

        fun newInstance(selectedMetric: AppSettings.MetricSystem): ChooseMetricDialog {
            val dialog = ChooseMetricDialog()
            dialog.arguments = Bundle().apply {
                putString(BUNDLE_KEY, selectedMetric.name)
            }

            return dialog
        }
    }
}

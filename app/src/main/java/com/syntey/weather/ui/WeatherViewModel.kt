package com.syntey.weather.ui

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.syntey.weather.AppConstants
import com.syntey.weather.data.network.response.Coord
import com.syntey.weather.domain.model.AppSettings
import com.syntey.weather.domain.model.Forecast
import com.syntey.weather.domain.model.Result
import com.syntey.weather.domain.model.Today
import com.syntey.weather.domain.repository.IAppStateRepository
import com.syntey.weather.domain.repository.ITextRepository
import com.syntey.weather.domain.usecase.ForecastWeatherUseCase
import com.syntey.weather.domain.usecase.ForecastWeatherUseCaseParams
import com.syntey.weather.domain.usecase.TodayWeatherUseCase
import com.syntey.weather.domain.usecase.TodayWeatherUseCaseParams
import com.syntey.weather.ui.base.BaseViewModel
import com.syntey.weather.ui.base.ViewState
import com.syntey.weather.ui.forecast.ForecastUi
import com.syntey.weather.ui.today.InfoCardUi
import com.syntey.weather.ui.today.TodayUi
import com.syntey.weather.util.SingleLiveEvent
import com.syntey.weather.util.Utils.parseError
import com.syntey.weather.util.subscribeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject
import timber.log.Timber

class WeatherViewModel @Inject constructor(
    private val todayWeatherUseCase: TodayWeatherUseCase,
    private val forecastWeatherUseCase: ForecastWeatherUseCase,
    private val appStateRepository: IAppStateRepository,
    private val textRepository: ITextRepository
) : BaseViewModel() {

    private val refresh: BehaviorProcessor<Unit> = BehaviorProcessor.create()
    private var currentLocation: Coord = Coord(
        AppConstants.DEFAULT_LOCATION_LONGITUDE,
        AppConstants.DEFAULT_LOCATION_LATITUDE
    )

    private val _forecastViewState = MutableLiveData<ViewState<List<ForecastUi>>>()
    val forecastViewState: LiveData<ViewState<List<ForecastUi>>> get() = _forecastViewState

    private val _todayViewState = MutableLiveData<ViewState<TodayUi>>()
    val todayViewState: LiveData<ViewState<TodayUi>> get() = _todayViewState

    private val _showLocationNotGrantedDialog = SingleLiveEvent<Unit>()
    val showLocationNotGrantedDialog: LiveData<Unit> get() = _showLocationNotGrantedDialog

    private var requestingLocationUpdates = false

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val locationCallback by lazy {
        object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                p0?.lastLocation?.let { updateLocation(it) }
            }
        }
    }

    init {
        populateDb()
        subscribeForToday()
        subscribeForForecast()
    }

    fun initLocation(context: Context) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    }

    fun permissionsGranted() {
        requestingLocationUpdates = true
        getLastLocation()
    }

    fun permissionsNotGranted() {
        locationFailed()

        _showLocationNotGrantedDialog.postValue(Unit)
    }

    fun onPause() {
        stopLocationUpdates()
    }

    fun onResume() {
        startLocationUpdates()
    }

    fun refreshTriggered() {
        refresh.onNext(Unit)
    }

    fun updateLocation(location: Location?) {
        currentLocation =
            if (location != null && (location.longitude != 0.0 || location.latitude != 0.0)) {
                Coord(
                    location.longitude,
                    location.latitude
                )
            } else {
                Coord(
                    AppConstants.DEFAULT_LOCATION_LONGITUDE,
                    AppConstants.DEFAULT_LOCATION_LATITUDE
                )
            }

        refreshTriggered()
    }

    fun locationFailed() {
        updateLocation(null)
    }

    private fun subscribeForToday() {
        refresh.switchMap {
            todayWeatherUseCase.invoke(
                TodayWeatherUseCaseParams(currentLocation.lat, currentLocation.lon)
            ).switchMap { resource ->
                appStateRepository.getMetricSystem().map { metricSystem ->
                    when (resource) {
                        is Result.Success -> _todayViewState.postValue(
                            ViewState.data(mapTodayToTodayUi(resource.data, metricSystem))
                        )
                        is Result.Error -> _todayViewState.postValue(
                            ViewState.error(parseError(resource.error, textRepository))
                        )
                        is Result.Loading -> _todayViewState.postValue(ViewState.loading())
                    }
                }
            }
        }.subscribeOn(Schedulers.io()).subscribe(
            {},
            { e ->
                Timber.e(e)
            }
        )?.let {
            disposables.add(it)
        }
    }

    private fun mapTodayToTodayUi(today: Today, metricSystem: AppSettings.MetricSystem): TodayUi {
        return TodayUi(
            city = today.city,
            temperature = when (metricSystem) {
                AppSettings.MetricSystem.METRIC -> String.format(
                    "%.0f°C",
                    today.temperature
                )
                AppSettings.MetricSystem.IMPERIAL -> String.format(
                    "%.0f°F",
                    today.temperature
                )
            },
            description = today.weatherDescription,
            icon = AppConstants.IMAGE_URL + today.weatherIcon + AppConstants.IMAGE_FILE_TYPE,
            infoCard = getInfoCard(today, metricSystem)
        )
    }

    private fun getInfoCard(today: Today, metricSystem: AppSettings.MetricSystem): InfoCardUi {
        return InfoCardUi(
            windSpeed = when (metricSystem) {
                AppSettings.MetricSystem.METRIC -> String.format("%.1f km/h", today.windSpeed * 3.6)
                AppSettings.MetricSystem.IMPERIAL -> String.format("%.1f mi/h", today.windSpeed)
            },
            rain = "${today.clouds}%",
            drops = "${today.rain} mm",
            pressure = "${today.pressure} hPa",
            windDeg = today.windDeg,
            sunrise = SimpleDateFormat("h:mm a", Locale.ENGLISH).format(today.sunrise)
        )
    }

    private fun subscribeForForecast() {
        refresh.switchMap {
            forecastWeatherUseCase.invoke(
                ForecastWeatherUseCaseParams(currentLocation.lat, currentLocation.lon)
            ).switchMap { resource ->
                appStateRepository.getMetricSystem().map { metricSystem ->
                    when (resource) {
                        is Result.Success -> {
                            val dateFormat = SimpleDateFormat("E h:mm a", Locale.ENGLISH)
                            _forecastViewState.postValue(
                                ViewState.data(resource.data.map { forecast ->
                                    mapForecastToForecastUi(forecast, metricSystem, dateFormat)
                                })
                            )
                        }
                        is Result.Error -> _forecastViewState.postValue(
                            ViewState.error(
                                parseError(
                                    resource.error,
                                    textRepository
                                )
                            )
                        )
                        is Result.Loading -> _forecastViewState.postValue(ViewState.loading())
                    }
                }
            }
        }.subscribeOn(Schedulers.io()).subscribe(
            {},
            { e ->
                Timber.e(e)
            }
        )?.let {
            disposables.add(it)
        }
    }

    private fun mapForecastToForecastUi(
        forecast: Forecast,
        metricSystem: AppSettings.MetricSystem,
        dateFormat: SimpleDateFormat
    ): ForecastUi {
        return ForecastUi(
            weatherDescription = "${forecast.weather} on ${dateFormat.format(forecast.time)}",
            temperature = when (metricSystem) {
                AppSettings.MetricSystem.METRIC -> String.format(
                    "%.0f°C",
                    forecast.temperature
                )
                AppSettings.MetricSystem.IMPERIAL -> String.format(
                    "%.0f°F",
                    forecast.temperature
                )
            },
            iconUrl = AppConstants.IMAGE_URL + forecast.weatherIcon + AppConstants.IMAGE_FILE_TYPE
        )
    }

    private fun populateDb() {
        appStateRepository.populateIfEmpty(AppSettings()).subscribeOn(Schedulers.io())
            .subscribeDisposable()
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(
            LocationRequest(),
            locationCallback,
            Looper.getMainLooper()
        )
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationClient
            .lastLocation
            .addOnSuccessListener { location: Location? ->
                updateLocation(location)
            }.addOnFailureListener { e ->
                Timber.e(e)
                locationFailed()
            }
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }
}

package com.syntey.weather.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.HasDefaultViewModelProviderFactory
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseFragment<VB : ViewBinding> : Fragment(), HasDefaultViewModelProviderFactory {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var _binding: VB? = null
    val binding get() = _binding!!

    abstract fun getBindings(inflater: LayoutInflater, container: ViewGroup?): VB

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = getBindings(inflater, container)
        return binding.root
    }

    override fun getDefaultViewModelProviderFactory(): ViewModelProvider.Factory {
        return viewModelFactory
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

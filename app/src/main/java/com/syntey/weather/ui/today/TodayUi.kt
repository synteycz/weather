package com.syntey.weather.ui.today

data class TodayUi(
    val city: String,
    val temperature: String,
    val description: String,
    val icon: String,
    val infoCard: InfoCardUi
)

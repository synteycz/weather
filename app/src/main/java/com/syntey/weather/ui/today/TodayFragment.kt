package com.syntey.weather.ui.today

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import coil.load
import com.syntey.weather.databinding.TodayFragmentBinding
import com.syntey.weather.ui.WeatherViewModel
import com.syntey.weather.ui.base.BaseFragment
import com.syntey.weather.ui.base.ViewState

class TodayFragment : BaseFragment<TodayFragmentBinding>() {

    private val viewModel by activityViewModels<WeatherViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeData()

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.refreshTriggered()
        }
    }

    private fun observeData() {
        viewModel.todayViewState.observe(viewLifecycleOwner, { viewState ->
            when (viewState) {
                is ViewState.Data -> {
                    setData(viewState.data)
                }
                is ViewState.Loading -> {
                    setLoading()
                }
                is ViewState.Error -> {
                    setError(viewState.error)
                }
            }
        })
    }

    private fun setData(
        data: TodayUi
    ) {
        binding.apply {
            swipeRefreshLayout.isRefreshing = false
            dataState.visibility = View.VISIBLE
            loadingState.visibility = View.GONE
            errorState.root.visibility = View.GONE

            cityNameText.text = data.city

            temperatureText.text = data.temperature

            weatherDescriptionText.text = data.description
            weatherDescriptionIcon.load(data.icon)

            data.infoCard.apply {
                rainText.text = rain
                dropsText.text = drops
                pressureText.text = pressure
                windDirectionText.text = windDeg
                windSpeedText.text = windSpeed
                sunriseText.text = sunrise
            }
        }
    }

    private fun setLoading() {
        binding.apply {
            loadingState.visibility = View.VISIBLE
            errorState.root.visibility = View.GONE
            dataState.visibility = View.GONE
        }
    }

    private fun setError(error: String) {
        binding.apply {
            swipeRefreshLayout.isRefreshing = false
            errorState.root.visibility = View.VISIBLE
            loadingState.visibility = View.GONE
            dataState.visibility = View.GONE

            errorState.errorMessage.text = error
        }
    }

    override fun getBindings(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): TodayFragmentBinding {
        return TodayFragmentBinding.inflate(inflater, container, false)
    }
}

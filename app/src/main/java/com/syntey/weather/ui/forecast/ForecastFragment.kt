package com.syntey.weather.ui.forecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.syntey.weather.databinding.ForecastFragmentBinding
import com.syntey.weather.ui.WeatherViewModel
import com.syntey.weather.ui.base.BaseFragment
import com.syntey.weather.ui.base.ViewState

class ForecastFragment : BaseFragment<ForecastFragmentBinding>() {

    private val viewModel by activityViewModels<WeatherViewModel>()

    private lateinit var adapter: ForecastAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpAdapter()
        observeData()

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.refreshTriggered()
        }
    }

    private fun observeData() {
        viewModel.forecastViewState.observe(viewLifecycleOwner, { viewState ->
            when (viewState) {
                is ViewState.Data -> {
                    setData(viewState.data)
                }
                is ViewState.Loading -> {
                    setLoading()
                }
                is ViewState.Error -> {
                    setError(viewState.error)
                }
            }
        })
    }

    private fun setData(data: List<ForecastUi>) {
        binding.apply {
            forecastCollection.visibility = View.VISIBLE
            loadingState.visibility = View.GONE
            errorState.root.visibility = View.GONE
        }

        adapter.submitList(data)
    }

    private fun setLoading() {
        binding.apply {
            loadingState.visibility = View.VISIBLE
            errorState.root.visibility = View.GONE
            forecastCollection.visibility = View.GONE
        }
    }

    private fun setError(error: String) {
        binding.apply {
            errorState.root.visibility = View.VISIBLE
            loadingState.visibility = View.GONE
            forecastCollection.visibility = View.GONE

            errorState.errorMessage.text = error
        }
    }

    private fun setUpAdapter() {
        adapter = ForecastAdapter()

        binding.forecastCollection.layoutManager = LinearLayoutManager(requireContext())
        binding.forecastCollection.adapter = adapter
    }

    override fun getBindings(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): ForecastFragmentBinding {
        return ForecastFragmentBinding.inflate(inflater, container, false)
    }
}

package com.syntey.weather.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.syntey.weather.ui.forecast.ForecastFragment
import com.syntey.weather.ui.today.TodayFragment

class PageAdapter(
    fragmentActivity: FragmentActivity,
    private val tabCount: Int
) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return tabCount
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> TodayFragment()
            1 -> ForecastFragment()
            else -> TodayFragment()
        }
    }
}

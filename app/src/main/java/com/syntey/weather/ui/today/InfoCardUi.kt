package com.syntey.weather.ui.today

data class InfoCardUi(
    val windSpeed: String,
    val rain: String,
    val drops: String,
    val pressure: String,
    val windDeg: String,
    val sunrise: String
)

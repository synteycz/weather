package com.syntey.weather.ui.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import com.syntey.weather.domain.model.AppSettings
import com.syntey.weather.domain.repository.IAppStateRepository
import com.syntey.weather.ui.base.BaseViewModel
import com.syntey.weather.util.subscribeDisposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
    private val appStateRepository: IAppStateRepository
) : BaseViewModel() {

    private val _showDialog: PublishProcessor<Unit> = PublishProcessor.create()
    val showDialog: LiveData<AppSettings.MetricSystem>
        get() = LiveDataReactiveStreams.fromPublisher(
            appStateRepository.getMetricSystem().switchMap { metricSystem ->
                _showDialog.map { metricSystem }
            }
        )

    val metricSystem: LiveData<AppSettings.MetricSystem> =
        LiveDataReactiveStreams.fromPublisher(appStateRepository.getMetricSystem())

    fun showDialog() {
        _showDialog.onNext(Unit)
    }

    fun dialogConfirmed(itemId: Int) {
        appStateRepository.updateMetricSystem(AppSettings.MetricSystem.values()[itemId])
            .subscribeOn(Schedulers.io()).subscribeDisposable {}
    }
}

plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroid)
    id(BuildPlugins.kotlinKapt)
}

android {
    compileSdkVersion(AndroidSdk.compile)
    buildToolsVersion = AndroidSdk.buildToolsVersion
    defaultConfig {
        applicationId = "com.syntey.weather"
        minSdkVersion(AndroidSdk.min)
        targetSdkVersion(AndroidSdk.target)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        manifestPlaceholders["screenOrientation"] = "unspecified"
        resValue("string", "apiKey", project.properties["apiKey"].toString())
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )

            manifestPlaceholders["screenOrientation"] = "portrait"
        }
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(BuildPlugins.kotlinStdLib)

    implementation(AndroidLibraries.appCompat)
    implementation(AndroidLibraries.core)
    implementation(AndroidLibraries.constraintLayout)
    implementation(AndroidLibraries.recyclerView)
    implementation(AndroidLibraries.design)
    implementation(AndroidLibraries.activity)
    implementation(AndroidLibraries.fragment)
    implementation(AndroidLibraries.swipeRefreshLayout)

    implementation(NetworkLibraries.retrofit)
    implementation(NetworkLibraries.retrofitMoshi)
    implementation(NetworkLibraries.retrofitRxJava)

    implementation(NetworkLibraries.loggingInterceptor)

    implementation(AndroidLibraries.locationServices)
    implementation(OtherLibraries.timber)

    implementation(Dagger.dagger)
    kapt(Dagger.daggerCompiler)
    implementation(Dagger.daggerSupport)
    kapt(Dagger.daggerAndroidProcessor)

    compileOnly(Dagger.javaXannotation)
    implementation(OtherLibraries.threeTenAbp)

    testImplementation(TestLibraries.core)
    testImplementation(TestLibraries.junit4)
    testImplementation(TestLibraries.truth)
    testImplementation(TestLibraries.mockk)
    testImplementation(TestLibraries.arch)

    implementation(AndroidLibraries.viewModel)
    implementation(AndroidLibraries.liveData)
    implementation(AndroidLibraries.reactiveStreams)

    implementation(Room.roomRuntime)
    implementation(Room.roomRxjava)
    kapt(Room.roomCompiler)

    implementation(RxJava.rxJava)
    implementation(RxJava.rxAndroid)

    implementation(OtherLibraries.coil)
}
